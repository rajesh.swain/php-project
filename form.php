<?php
include("connection.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <style>
     
        label{
            font-weight: bold;
        }
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="./form.css">

</head>
<body >
    <div class="container">
        <form action="#" method="POST" class="form-inline" >
        <div class="title form-group">
            <h1 style="text-align: center;"><b>Register form</b></h1>
        </div>
        <div class="form ">
            <div class="input_field ">
            <i class="fa fa-address-book" style="font-size:36px"></i>
                <label for="">First Name</label>
               
                <input type="text"  class="input form-control" name="fname"placeholder='First Name' required>
            </div>
            <div class="input_field">
            <i class="fa fa-address-book" style="font-size:36px"></i>
                <label for="">Last Name</label>
                <input type="text" class="input form-control" name="lname"placeholder='Last Name' required>
            </div>
            <div class="input_field">
            <i class="fa fa-lock" style="font-size:36px"></i>
                <label for="">Password</label>
                
                <input type="password" class="input form-control" name="password"placeholder=' Password' required>
            </div>
            <div class="input_field">
            <i class="fa fa-lock" style="font-size:36px"></i>
                <label for="">Confirm Password</label>
                <input type="password" class="input form-control"placeholder='Confirm Password' name="conpassword" required>
            </div>
                
                

            <div class="input_field">
                <label for="">Gender</label>
                <select name="gender" id="">
                    <option value="not selected">select</option>
                    <option value="male" >male</option>
                    <option value="female">female</option>
                </select>
            </div>
            
            
            <div class="input_field">
            <i class="fa fa-envelope" style="font-size:36px"></i>
                <label for="">Email </label>
                <input type="text" class="input form-control"placeholder='Email'name="email" required>
            </div>

            <div class="input_field">
            <i class="fa fa-phone" style="font-size:20px"></i>
                <label for="">Phone </label>
                <input type="text" class="input form-control"placeholder='Phone'name="phone" maxlength="10" required>
            </div>
           
            <div class="input_field">
                <label for="">Address</label>
                <textarea name="address" id="" cols="20" rows="2"placeholder='Address' class="textarea" required></textarea>
            </div>
            <div class="input_field terms">
                <label for="" class="check">
                    <input type="checkbox">
                    
                    <span class="checkmark"></span>
                </label>
                <p>agree to terms and conditions</p>
                
            </div>
            <div class="input_field btn btn-primary">
                <input style="color: WHITE;" type="submit" value="Register" class="btn" name="register">
            </div>
            
        <br><br>
        Have an account?
        <a href="login.php">login here</a> 
            
        </div>
        </form>
    </div>
</body>
</html>


<?php
if($_POST['register'])
{
   $fname= $_POST['fname'];
   $lname= $_POST['lname'];
   $pwd= $_POST['password'];
   $cpwd= $_POST['conpassword'];
   $gender= $_POST['gender'];
   $email= $_POST['email'];
   $phone= $_POST['phone'];
   $address= $_POST['address'];

   if($fname!="" && $lname !="" && $pwd !="" && $cpwd !="" && $gender !="" && $email !="" && $phone !="" && $address !="")
   {
    

    $query= "insert into form (fname,lname,password,cpassword,gender,email,phone,address) 
                        values('$fname','$lname','$pwd','$cpwd','$gender','$email','$phone','$address')";
    $data=mysqli_query($conn,$query);
    if($data){
        // echo "<span style='color:blue';>data inserted";
        header('location:login.php');
        
      
                    
        
        
    }else{
        echo "fail the data inserted";
    }
}else{
    echo "<span style='color:red';> Oho! please fill the form";
    
}
}
?>



<!-- <a href='login.php ?email=$result[email] & password=$result[password]'><input type='submit' name="submit"></a> -->

